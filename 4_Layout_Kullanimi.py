# -*- coding: utf-8 -*-
from PyQt4.QtGui import *
 
uygulamam = QApplication([])

pencere = QWidget()
pencere.resize(300, 100) # Yeni boyutlar
pencere.move(50, 50) # Sol üst köşeden uzaklık

etiket = QLabel('PythonDersleri.com',pencere)
buton = QPushButton('Butonum',pencere)
yazi_text = QTextEdit("Text Biseyler") 

yatayKutu = QHBoxLayout()
yatayKutu.addWidget(etiket)
yatayKutu.addWidget(buton)
yatayKutu.addWidget(yazi_text)
 
pencere.setLayout(yatayKutu)
pencere.setWindowTitle('Programım')
pencere.show()
 
uygulamam.exec_()