# -*- coding: utf-8 -*-
from __future__ import division
from PyQt4.QtGui import *
from PyQt4.QtCore import *
 
 
class ortalamaHesapla(QWidget):
    def __init__(self, parent=None):
        super(ortalamaHesapla, self).__init__(parent)
        #miras aldığım sınıfın constructorunu super() metodu ile veya aşağıdaki gibi de tanımlanabilir
        #QWidget.__init__(self)

        self.metin = "<center>Sinav Ortalamasi Hesaplama</center>"
 
        self.grid = QGridLayout()
        self.baslik = QLabel(self.metin)
        self.vizeA = QLabel('1. Vize Notunuz ')
        self.vizeB = QLabel('2. Vize Notunuz ')
        self.final = QLabel('Final Notunuz ')
        self.vizeAdeger = QLineEdit()
        self.vizeBdeger = QLineEdit()
        self.finalDeger = QLineEdit()
        self.sonuc = QLabel('Ortalama ')
        self.sonucDeger = QLabel('0.00')
        self.hesapla = QPushButton('Hesapla')
 
        self.grid.addWidget(self.baslik, 0, 0, 1, 2)
        self.grid.addWidget(self.vizeA, 1, 0)
        self.grid.addWidget(self.vizeAdeger, 1, 1)
        self.grid.addWidget(self.vizeB, 2, 0)
        self.grid.addWidget(self.vizeBdeger, 2, 1)
        self.grid.addWidget(self.final, 3, 0)
        self.grid.addWidget(self.finalDeger, 3, 1)
        self.grid.addWidget(self.sonuc, 4, 0)
        self.grid.addWidget(self.sonucDeger, 4, 1)
        self.grid.addWidget(self.hesapla, 5, 0, 1, 2)
 
        self.connect(self.hesapla, SIGNAL('pressed()'), self.hesapYap)
 
        self.setLayout(self.grid)
        self.setWindowTitle("Not Hesaplama ~ PythonDersleri")
 
    def hesapYap(self):
        ortalama = 0
        try:
            ilk_vize = int(self.vizeAdeger.text())
            ikinci_vize = int(self.vizeBdeger.text())
            final_notu = int(self.finalDeger.text())
            ortalama = (ilk_vize + ikinci_vize + final_notu)/3
        except:
            self.sonucDeger.setText('<span style="color: red;">Tam sayi giriniz!</span>')
            return
        self.sonucDeger.setText('<b>%0.2f</b>' % ortalama)
 
 
uyg = QApplication([])
pencere=ortalamaHesapla()
pencere.show()
uyg.exec_()